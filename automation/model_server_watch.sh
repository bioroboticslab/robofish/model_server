inotifywait -m ~/blubber_workspace/model_server/public_html/ -e create -e moved_to -e modify -e attrib -e close_write -e delete |
while read dir action file; do
    if [[ ! "$file" =~ ".+\.swp" ]]; then
        echo  "$action happened to $file, copying everything"
        rsync -a --delete /home/andi/blubber_workspace/model_server/public_html/ login_zedat:public_html/model_server/
    fi
done
